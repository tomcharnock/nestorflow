#!pip install https://github.com/tomcharnock/NestorFlow/archive/master.zip
#--------------------------------------------------------------------------
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
from nestorflow.nested import nested_sample, evidence, samples
import matplotlib.pyplot as plt

#| Global variables -- these would eventually be inputs to the sampler:
#| * n_dims: Number of dimensions
#| * n_live: Number of live points
#| * n_repeats: Number of repeats
#| * n_compress: Number of times to run a nested sampling compression
#| * likelihood: the likelihood to sample over L : [0,1]^D -> (-inf,inf)

n_dims = 20
n_live = 100
n_repeats = n_dims*2

def likelihood(x):
    dist = tfp.distributions.MultivariateNormalDiag(loc=(np.ones(n_dims)*0.5).astype(np.float32), scale_diag=(np.ones(n_dims)*0.1).astype(np.float32))
    return dist.log_prob(x)


#| Run nested sampling
points, L = nested_sample(likelihood, n_live, n_dims, n_repeats)
s, w = samples(points, L)

sess = tf.Session()
sess.run(tf.global_variables_initializer())
p, l, S, W = sess.run([points, L, s, w])

#| Plot the weights
W-=W.max()
plt.plot(np.exp(W))
plt.xlabel('iteration')
plt.ylabel('posterior weight')

#| Plot the samples
j = np.exp(W)>np.random.rand(len(W))
plt.plot(S[j,0], S[j,1], '.')
plt.xlabel('x')
plt.ylabel('y')

#| Plot the paths of some single live point runs
plt.plot(p[:,:2,0], p[:,:2,1], '-')
plt.xlabel('x')
plt.ylabel('y')

#| Plot the threads
i = np.empty(l.size)
i[np.argsort(l, axis=None)] = np.arange(l.size)
i = i.reshape(l.shape)
plt.plot(i, 'k', linewidth=0.5)
plt.xlabel('age')
plt.ylabel('loglikelihood')
