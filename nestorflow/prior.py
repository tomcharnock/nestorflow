import tensorflow_probability as tfp

def prior_sample(likelihood, n_live, n_dims):
    live_points = tfp.distributions.Uniform().sample((n_live, n_dims))
    L = likelihood(live_points)
    return live_points, L

