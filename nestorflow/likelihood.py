import tensorflow as tf

def wrap_likelihood(likelihood):
    def truncated_likelihood(x):
        too_big = tf.reduce_any(tf.greater(x, 1.), axis=1)
        too_small = tf.reduce_any(tf.less(x, 0.), axis=1)
        out = tf.logical_or(too_big, too_small)
        l = likelihood(x)
        return tf.where(out, tf.log(tf.zeros_like(l)), l)
    return truncated_likelihood

